let express = require('express')

const app = express()
let axios = require('axios')
const requestTimeMiddleware = require("./middelware/requestTime")


app.use(requestTimeMiddleware.logger)
app.use(requestTimeMiddleware.middleWData)
app.use(requestTimeMiddleware.middleWFile("hello.txt"))

app.get('/', async function (req, res){    
    let axiosResp = await axios.get("http://api.open-notify.org/astros.json")
    let names = axiosResp.data.people.map( personn => personn.name)
    let html = "<ul>"+ names.reduce ((accu, name) => accu + "<li>"+name+ "</li>","")+ "</ul>"
    res.send(html)
 })

app.listen(8080, function() {
    console.log("server listening on port 8080")
})