
exports.up = function(db) {
    return db.schema.createTable('User', table => {
        table.increments('id').unsigned().primary()
        table.string('fullname').notNull()
        table.string('email').notNull()

    }).createTable('Picture', table => {
        table.increments('id').unsigned().primary()
        table.integer('user_id').references('id').inTable('users').notNull().onDelete('cascade')
        table.dateTime('createdAt').notNull()
        table.string('path').notNull()
    })


  
}

exports.down = function(db) {
  
}
