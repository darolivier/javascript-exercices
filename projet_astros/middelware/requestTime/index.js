const fsp = require("fs").promises
const express = require("express")


let logger = function(request, res, next){
    console.log('method', request.method)
    console.log('url', request.url)
    next()
}

let middleWData = function(request, res, next){
    request.datelog = new Date()     //j ai choisi le nom datelog
    console.log(request.datelog)
    next()
}

let middleWFile = function(filepath){
    return function(req, res, next){
    console.log(filepath)
    fsp.appendFile(filepath,"date:"+req.datelog + "method:"+req.method + "url:" +req.url)//, e => console.log("fichier écrit"));
    next()
    }

}


exports.middleWData = middleWData
exports.middleWFile = middleWFile
exports.logger = logger

