// without promises
const oldstyle = (successCb, errorCb) => {
    console.log('stuff done')
    return (Math.random() > .5) ? successCb('yes') : errorCb('nope')
  }
  
  // call AND exploit result at the same time
  oldstyle(msg => console.log(msg), err => console.error(err))
  
  // with promises
  const newstyle = () => new Promise( (resolve, reject) => {
    console.log('stuff done')
    return (Math.random() > .5) ? resolve('yes') : reject('nope')
  })
  
  // call then exploit results, 2 separate steps
  const prom = newstyle() 
  prom.then(msg => console.log(msg), err => console.log(err))