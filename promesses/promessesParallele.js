const fs = require("fs")
const promise = require("promise")


const darksizeofthedir = (path) => new Promise((resolve, rejected) => {
    fs.readdir(path, (err, list) => {
        Promise.all(list.map(filesize))                    //filesize dans autre exercice faire un require
            .then(sizes => sizes.map((size,i) => ({size:size, name: list[i]})))
            .then(aofo => aofo.sort((o, p) => (o.size > p.size) ? -1 : 1))
            .then(resolve) //on aurait pu ecrire resolve( ligne 7 Promise.all etc.)

//res.sort((o, p) => (o.size > p.size) ? return -1 : 1)

    })
})

darksizeofthedir(.)


//let ap = a.map(filesize)

// >ap
// [Promise {35}, Promise {0}, Promise {225}]
// > promise.all(ap).then (sizes => sizes.map( (size, i) => ({size: size, name:a[i]}))