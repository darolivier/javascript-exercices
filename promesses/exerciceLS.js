

/* taille ls sous forme callback
function sizefile(path, cb) { fs.stat(path, (err, stat) => {if (err) 
return cb(err) 
cb(null, stat.size)
})}
on l'appelle:
sizefile('test', (err, size)=> {if (err) return console.log(err))
console.log(size)})

*/

//const promise = require("promise")
const fs = require("fs")
//sizefile version promesse

/*const fileSize = (filename) => new Promise( (result, rejected) => {  
         fs.statSync(filename, (err, res) => {
            if (err) return rejected(err)
        return result(res.size)
    })
})*/

const numberofTs = (filename) => new Promise((resolve, reject) => {
    fs.readFile(filename,'utf8', (err, res) => {
        if (err) return reject(err)
    return resolve(res.split('').filter( el => el === 't').length)})
})
const prom = numberofTs("test.txt")
prom.then(msg => console.log(msg)).catch(e => console.log(e))




/*

const prom = fileSize()
prom.then(msg => console.log(msg)).catch(e => console.log(e))

fileSize('home/olivier/workspaces/promesses/test.txt')
*/