
exports.up = function(knex) {

    return knex.schema.createTable('User', table => {
         table.increments('id').unsigned().primary()
         table.string('email').unique().notNullable()
         table.string('password')
         table.boolean('active').defaultTo(false)
         table.boolean('is_admin').defaultTo(false)
         table.string('fullname').notNull()
 
     }).createTable('Picture', table => {
         table.increments('id').unsigned().primary()
         table.integer('user_id').references('id').inTable('User').notNull().onDelete('cascade')
         table.timestamp('created_at').defaultTo(knex.fn.now())
         table.string('path').notNull()
     })
 }
 
 exports.down = function(knex) {
 }
 