
//auth0/node-jsonwebtoken/github

const express = require("express")
const jwt = require('jsonwebtoken')
const app = express()


app.get('/api', (req, res) => {
    res.json({
        message: 'welcome to the api'
    })
})

app.post('/api/posts', verifyToken, (req, res)=> {
    jwt.verify(req.token, 'secretkey', (err, authData)=> {
        if(err) {res.sendStatus(403)
        } else {
            res.json({
                message:'post created...',
                authData
            })
        }
    })
    res.json({
        message: 'post created...'
    })
})

//creation page login

app.post('/api/login', (req, res) => {
    //create user pour l'exemple, normalement pas comme ça (db)
    const user = {
        id:1,
        username: 'olivier',
        email:'olivier@gmail.com'
    }
    
    jwt.sign({user}, 'secretkey', (err, token) => {
        res.json({token})
        
    })
})
//FORMAT OF TOKEN
//Authorization: bearer<access_token>


//verify token 
function verifyToken(req, res, next){
    //get auth header value
    const bearerHeader = req.headers['authorization']
    //check if bearer is undefined
    if(typeof bearerHeader !== 'undefined'){
        // Split at the space
        const bearer = bearerHeader.split(' ')
        //get token from array
        const bearerToken = bearer[1]
        //set the token
        req.token = bearerToken
        next()

    }else {
        res.sendStatus(403)
    }

}

