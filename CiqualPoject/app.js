const express = require('express')
const knex = require('knex')
const jwt = require('jsonwebtoken')
const app = express()
const bcrypt = require('bcrypt')
const axios = require("axios")

//////knex services (create find etc...)
let config = require('./knexfile.js')
let db = knex(config.development)



// Turn on JSON parser for REST services
app.use(express.json())
// Turn on URL-encoded parser for REST services     ////////FICHIER
app.use(express.urlencoded({ extended: true })) //////quand req formulaire submit de html transforme en body les donnees accessibles avec req.body.email
// Enable REST services                         //////app.post('create-acount'(dans action ="submit de html)"), middleware)
                                                /////creer user, creer jwt, token email, envoyer une reponse enregistrement effectuer



//function reaction to html submit buttom   
app.post("/create-account", async function (req, res) {
  if (!req.body.mail || !req.body.name) {
    res.sendStatus(400)
}
//////////body'request created by app.use(express.urlencoded) l.15. insertUser will save email and name into db
  try {
  let insertUser = await db.into('User').insert({email:req.body.email, fullname: req.body.name})
  }
  catch (err) {
    console.log(err)
    return res.sendStatus(400)
  } 
  //////setting of token will return a string returning header, payload and signature into var token
  let token = jwt.sign({fullname: req.body.name, email:req.body.email}, 'MON_CODE_SECRET')
  console.log(`localhost:3000/confirm-registration?token=${token}`) 
})
app.get(('/confirm-registration'), (req, res) => {
  jwt.verify(req.query.token, 'MON_CODE_SECRET', (err, decoded) =>{
    if (err) {
    res.sendStatus(400) 
  }
  res.send(`<form method='POST' action = 'activate-account'>
            <h1>Bienvenue!!! ${decoded.fullname}</h1>
            <h2>${decoded.email}</h2>
            <label for='password'>Mot de Passe:<label>
            <input type='password' id='pass' name='pass' placeholder='votre mot de passe'>
            <input type=hidden name='email' value='${decoded.email}'}
            <button type='sbmit'>Submit</button>
            </form>`)
})
})
app.post('/activate-account', async (req, res) => {
  let saltRounds = 10
  let motDePasse = bcrypt.hashSync(req.body.pass, saltRounds)
  await db.into('User').update({active: true, password: motDePasse }).where({email: req.body.email})
  res.redirect('/loginxhr.html')
})

 app.post('/login', async (req, res, next) => {
    let [loginOk] = await db.select('*').from('User').where({email: req.body.email});
  if (loginOk.length === 0) {
    res.statusCode(403)
  }
  else{
    let saltRounds = 10
    console.log(req.body.password,loginOk.password )
    let motPasse = bcrypt.compareSync(req.body.password, loginOk.password)
    if (motPasse===true){
      res.status(200)
      res.redirect('home.html')
  }else{
      res.sendStatus(403)
      console.log("res.error")
    }
  } 
})

app.use(express.static("./staticfiles"))

//app.use(autorisationMiddleware)

// Start the server.
const port = process.env.PORT || 3000

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
  })
  //pour la suite  faire res.cookie
  //INSTALLER COOKIE PARSER
  //App.use(bodypareser.json)
  //app.use(bodyparser.ecoded)